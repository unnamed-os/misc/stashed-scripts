#!/usr/bin/env bash

set -euo pipefail

source envsetup.sh

export TARGET=i686-elf
export BINUTILS_ROOT="$SOURCE_ROOT/binutils"
export BINUTILS_SOURCE="$BINUTILS_ROOT/source"
export BINUTILS_BUILD="$BINUTILS_ROOT/build"
export GCC_ROOT="$SOURCE_ROOT/gcc"
export GCC_SOURCE="$GCC_ROOT/source"
export GCC_BUILD="$GCC_ROOT/build"
export BINUTILS_VERSION="2_36"
export GCC_VERSION="10"

export CPUS=$(nproc --all)

mkdir -p "$PREFIX"
mkdir -p "$BINUTILS_ROOT"

git clone git://sourceware.org/git/binutils-gdb.git -b binutils-$BINUTILS_VERSION-branch "$BINUTILS_SOURCE"

echo "Building binutils"
mkdir -p "$BINUTILS_BUILD"
pushd "$BINUTILS_BUILD"

"$BINUTILS_SOURCE/configure" \
	--target=$TARGET \
	--prefix="$PREFIX" \
	--with-sysroot \
	--disable-nls \
	--disable-werror

make -j$CPUS
make install

popd

echo
echo "Building GCC"

if ! which -- $TARGET-as; then
	echo "$TARGET-as not in PATH!"
	exit 1
fi

git clone git://gcc.gnu.org/git/gcc.git -b releases/gcc-$GCC_VERSION "$GCC_SOURCE"

mkdir -p "$GCC_BUILD"
pushd "$GCC_BUILD"
"$GCC_SOURCE/configure" \
	--target=$TARGET \
	--prefix="$PREFIX" \
	--disable-nls \
	--enable-languages=c,c++ \
	--without-headers

make -j$CPUS all-gcc
make -j$CPUS all-target-libgcc
make install-gcc
make install-target-libgcc

echo
echo "Done."
echo
echo "Testing newly built cross-compiler..."

echo "Path: $(which $TARGET-gcc)"
$TARGET-gcc --version


